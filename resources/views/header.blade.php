
<div class="site-head">
<a href="/" id="logo"><img class="logo" src="{{ asset('images/logo.jpg') }}" alt="Library Logo"></a>
<h3 id="site-title">Library of the Society of Colorado Sun</h3>
</div>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="/books">Browse Books</a>

    {{ Form::open(['method' => 'GET', 'url' => 'search', 'class' => "form-inline"]) }}
    <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
    {{ Form::submit('Search', ['class' => 'btn btn-outline-success my-2 my-sm-0', 'type' => 'submit']) }}
    {{ Form::close() }}

    <a href="{{ URL::to('books/create') }}">
        <button type="button" class="btn btn-dark">Leave a Book</button>
    </a>
</nav>