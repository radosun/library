@extends('layout')

@section('content')

    <div class="title">
        <h3>Edit Book</h3>
    </div>



    <div class="form-bg" style="background-image: url('{{ substr($book->image, 6 )}}')">
        <div class="form">

            {{ Form::model($book,
            [
            'files' => true,
            'method' => 'put',
            'route' => ['books.update', $book]
            ])
            }}

            {{ Form::text('title') }}
            {{ Form::text('author') }}
            {{ Form::text('publication_date', $book->publication_date, ['id' => 'datepicker', 'class' => 'date form-control']) }}
            {{ Form::file('image') }}
            {{ Form::submit('Edit Book', ['class' => 'btn btn-primary']) }}

            {{ Form::close() }}




        </div>
    </div>

    @if(count($errors))
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@stop