@extends('layout')

@section('content')

    @if (empty($books))
        <h3>No Books Available</h3>
    @endif

    <div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">@sortablelink('title')</th>
                    <th scope="col">@sortablelink('author')</th>
                    <th scope="col">Publication date</th>
                    <th scope="col">View</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
            @foreach($books as $book)
                <tr>
                    <td><img class="img-thumbnail" src="{{ substr($book->image, 7 )}}" alt="book image"></td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->author }}</td>
                    <td>{{ $book->publication_date }}</td>
                    <td><a href="{{ URL::to('books/'. $book->id) }}" class="btn btn-success">View</a></td>
                    <td><a href="{{ URL::to('books/' . $book->id . "/edit") }}" class="btn btn-info">Edit</a></td>
                    <td>
                        {{ Form::open(['url' => 'books/' . $book->id, 'class' => 'pull-right']) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $books->appends(\Request::except('page'))->render() }}
    </div>
@stop