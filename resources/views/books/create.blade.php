@extends('layout')

@section('content')

    <div class="title">
        <h3>Add a New Book</h3>
    </div>



    <div class="form-bg" style="background-image: url('/uploads/books/default.jpg')">
        <div class="form">
            {{ Form::open(['route' => 'books.store', 'method' => 'post', 'files' => true]) }}

            {{ Form::text('title', '',
            ['placeholder' => 'Book Title', 'class' => 'form-control', 'required']) }}
            {{ Form::text('author', '',
            ['placeholder' => 'Author', 'class' => 'form-control']) }}
            {{ Form::text('publication_date', '',
                ['id' => 'datepicker', 'placeholder' => 'Publication Date', 'class' => 'date form-control']) }}
            {{ Form::label('book-cover', 'Book Cover 2MB Max') }}
            {{ Form::file('image', ['class' => 'image form-control', 'name' => 'image']) }}

            {{ Form::submit('Add Book', ['class' => 'btn btn-primary']) }}
            {{  Form::close()  }}


        </div>
    </div>

    @if(count($errors))
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@stop