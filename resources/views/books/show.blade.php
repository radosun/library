@extends('layout')

@section('content')

    <div class="title">
        <h3>{{ $book->title }}</h3>
        <h4>{{ $book->author }}</h4>
        <h4>{{ $book->publication_date }}</h4>
    </div>



    <div class="form-bg" style="background-image: url('{{ substr($book->image, 6 )}}')">
        <div class="form">

        </div>
    </div>

    @if(count($errors))
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@stop