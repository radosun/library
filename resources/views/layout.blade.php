<!doctype html>
<html lang="en">
<head>
    @include('head')
</head>
<body>
<div class="container">
    <header>
        @include('header')
    </header>

    <div id="main" class="row">

        @yield('content')

    </div>

    <footer>
        @include('footer')
    </footer>
</div>


</body>
</html>