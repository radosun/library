<?php

namespace App;
use Kyslik\ColumnSortable\Sortable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use Sortable;
    use SearchableTrait;

    protected $fillable = ['title', 'author', 'image', 'publication_date'];

    protected $searchable = [
      'columns' => [
          'books.title' => 3,
          'books.author' => 2,
      ]
    ];

    public $sortable = ['title', 'author'];

}
