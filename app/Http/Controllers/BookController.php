<?php

namespace App\Http\Controllers;


use App\Book;
use Image;
use Session;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::sortable()->paginate(5);
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
            'publication_date' => 'required'
        ]);

        $americanDate = str_replace('-', '/', $request->publication_date);
        $date = date("Y-m-d", strtotime($americanDate));


        $book = new Book;

        $book->title = $request->title;
        $book->author = $request->author;
        $book->publication_date = $date;

        $imgCheck = $request->hasFile('image');

        if($imgCheck){
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(678, 1082)->save( 'uploads/books/' . $imageName);
            $book->image = 'public/uploads/books/' . $imageName;
        }


        $book->save();

        Session::flash('message', 'Successfully Added Book!');

        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
            'publication_date' => 'required'
        ]);

        $date = '';

        if($book->publication_date == $request->publication_date){
            $date = $book->date;
        } else {
            $americanDate = str_replace('-', '/', $request->publication_date);
            $date = date("Y-m-d", strtotime($americanDate));
        }

        $book->title = $request->title;
        $book->author = $request->author;


        $imgCheck = $request->hasFile('image');

        if($imgCheck){
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(678, 1082)->save( 'uploads/books/' . $imageName);
            $book->image = 'public/uploads/books/' . $imageName;
        }


        $book->save();

        Session::flash('message', 'Successfully Updated Boook');

        return redirect()->route('books.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('/books');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $books = Book::search($search, null, true)->get();
        return view('books.search', compact('books'));
    }
}
