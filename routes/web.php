<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('books', 'BookController')->only([
   'index', 'create', 'store', 'update', 'edit', 'show', 'destroy'
]);

Route::get('/search', 'BookController@search');