<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'author' => $faker->firstName,
        'image' => $faker->image( '/public/uploads/books', $width = '678', $height = '1082'),
        'publication_date' => $faker->date
    ];
});
