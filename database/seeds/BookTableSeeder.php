<?php
/**
 * Created by PhpStorm.
 * User: radosun
 * Date: 4/26/18
 * Time: 8:10 PM
 */

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Book;


class BookTableSeeder extends Seeder
{
    public function run()
    {
//        factory(App\Book::class, 50)->make();
        $faker = Faker::create();

        for($i = 1; $i <= 50; $i++){
            $title = $faker->sentence($nb = 4, $variableNbWords = true);
            $author = $faker->firstName . " " . $faker->lastName;
            $publicationDate = $faker->date;

            Book::create([
                'title' => $title,
                'author' => $author,
                'publication_date' => $publicationDate,
                'image' => $faker->image($dir = 'public/uploads/books', $width = '678', $height = '1082'),
            ]);
        }
    }
}